package tv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class BestBuyScraper {
	public static void main(String[] args) {
		try {
			int maxPages = 1;
			List<TV> tvs = new ArrayList<>();
			for (int i = 1; i <= maxPages; i++) {
				URL url = new URL("https://www.bestbuy.ca/en-ca/category/televisions/21344.aspx?type=product&page=" + i
						+ "&sortBy=price&sortDir=asc&filter=category%253aTV%2B%2526%2BHome%2BTheatre%253bcategory%253aTelevisions%253btelevisionsize0enrchrange%253a50%2B-%2B59%2BInches%257c60%2B-%2B69%2BInches");

				// find pages

				Document doc = Jsoup.parse(url, 10000);
				assert doc != null;
				Element pagesList = doc.selectFirst(
						"#ctl00_CP_ctl00_ctl01_ProductSearchResultListing_bottomPaging > div > div.pagination-control-wrapper.text-center > ul");

				assert pagesList != null;
				Elements pages = pagesList.select("[data-page]");
				System.out.println("pages size:" + pages.size());

				if (i == 1) {
					int temp = 0;
					for (Element page : pages) {
						if (page.text().matches("\\d")) {
							temp = Integer.parseInt(page.text());
							if (temp > maxPages) {
								maxPages = temp;
							}
						}
					}
					System.out.println("Actual pages: " + maxPages);
				}

				Element elements = doc
						.selectFirst("#ctl00_CP_ctl00_ctl01_ProductSearchResultListing_SearchProductListing>ul");

				Elements rows = elements.select(".listing-item");
				System.out.println(rows.size());
				for (Element row : rows) {
					Elements infos = row.select(".prod-info");
					for (Element info : infos) {
						TV tv = TV.build(info);
						tvs.add(tv);
					}
				}
			}

			System.out.println("TVs found: " + tvs.size());
			File csv = new File("tvs.csv");
			if (!csv.exists()) {
				csv.createNewFile();
			}
			try (BufferedWriter out = new BufferedWriter(new FileWriter(csv))) {
				out.write(TV.csvHeaders());
				out.newLine();
				for (TV tv : tvs) {
					System.out.println(tv);
					out.write(tv.toCSV());
					out.newLine();
				}
				out.flush();
			} finally {
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
