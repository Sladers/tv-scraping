package tv;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class TV {
	private double price;
	private String title;
	private String brand;
	private int size;
	private boolean fourK;
	private boolean refurbished;
	private boolean hdr;
	private int year;
	private String link;

	public static final Pattern BRAND_SIZE = Pattern.compile("(.*)(\\d\\d+)(\"|\'\'| inch|-inch|�|\\.6\")",
			Pattern.CASE_INSENSITIVE);
	public static final Pattern INCH = Pattern.compile("\"|\'\'| inch|-inch", Pattern.CASE_INSENSITIVE);
	public static final Pattern REFURBISHED = Pattern.compile("Refurbished|Open", Pattern.CASE_INSENSITIVE);

	public static String csvHeaders() {
		return "Brand,Size,Price,Year,4K/UHD,HDR,Refurbished,Description,Link";
	}

	public String toCSV() {
		StringBuilder sb = new StringBuilder();

		sb.append(brand);
		sb.append(",");
		sb.append(size);
		sb.append(",");
		sb.append(price);
		sb.append(",");
		sb.append(year);
		sb.append(",");
		sb.append(fourK);
		sb.append(",");
		sb.append(hdr);
		sb.append(",");
		sb.append(refurbished);
		sb.append(",");
		sb.append(title.replaceAll(",", ""));
		sb.append(",");
		sb.append(link);

		return sb.toString();
	}

	public static TV build(Element e) {
		TV tv = new TV();
		Element linkElement = e.selectFirst("a");
		if (linkElement != null) {
			tv.link = "http://www.bestbuy.ca" + linkElement.attr("href");
			try {
				Document doc = Jsoup.parse(new URL(tv.link), 10000);
				Element field = doc.selectFirst("#ctl00_CP_ctl00_pdpDynamicContent_Tab_specs_repProductSpecs_ctl01_LN");
				if (field != null && "Model Year".equalsIgnoreCase(field.text())) {
					Element yearElement = doc
							.selectFirst("#ctl00_CP_ctl00_pdpDynamicContent_Tab_specs_repProductSpecs_ctl01_LV");
					String text = yearElement != null ? yearElement.text() : null;
					if (text != null && text.matches("\\d+")) {
						tv.year = Integer.parseInt(text);
					}
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		String price = e.selectFirst(".amount").text();

		if (price != null) {
			tv.price = Double.parseDouble(price.replaceAll("\\$|,", ""));
		}

		tv.title = e.selectFirst(".prod-title>a").text();
		Matcher m = BRAND_SIZE.matcher(tv.title);
		if (m.find()) {
			tv.brand = m.group(1);
			tv.size = Integer.parseInt(m.group(2));

			for (Brand b : Brand.values()) {
				m = b.pattern.matcher(tv.brand);
				if (m.find()) {
					tv.brand = m.group();
					break;
				}
			}

		}
		tv.fourK = tv.title.contains("4K") || tv.title.contains("UHD");
		tv.refurbished = REFURBISHED.matcher(tv.title).find();
		tv.hdr = tv.title.contains("HDR");
		return tv;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "TV[price=" + price + ", brand=" + brand + ", size=" + size + ", fourK=" + fourK + ", refurbished="
				+ refurbished + ", title=" + title + "]";
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isFourK() {
		return fourK;
	}

	public void setFourK(boolean fourK) {
		this.fourK = fourK;
	}

	public boolean isRefurbished() {
		return refurbished;
	}

	public void setRefurbished(boolean refurbished) {
		this.refurbished = refurbished;
	}
}
