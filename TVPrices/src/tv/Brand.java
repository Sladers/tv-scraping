package tv;

import java.util.regex.Pattern;

public enum Brand {
	Haier(), //
	HISENSE(), //
	Insignia(), //
	LG(), //
	Panasonic(), //
	Philips(), //
	RCA(), //
	Samsung(), //
	Sharp(), //
	Sony(), //
	SunBrite(), //
	TCL(), //
	Toshiba(), //
	VIZIO(),//
	;

	private Brand() {
		pattern = Pattern.compile(name(), Pattern.CASE_INSENSITIVE);
	}

	public final Pattern pattern;
}
